package SD.grpc;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static java.lang.Integer.parseInt;

public class Buton implements ActionListener {
    private String intake;
    private JLabel l, time;
    private String nume;
    private int pacient;

    public Buton(String intake, JLabel time, JLabel l, String nume, int pacient) {
        this.intake = intake;
        this.time = time;
        this.l = l;
        this.nume = nume;
        this.pacient = pacient;
    }


    public void actionPerformed(ActionEvent event) {


        if(l.getText().toString().equals("")){
            System.out.println("am ajuns aici boi" + time.getText());
            String[] timeList = time.getText().split(":");
            if(intake.equals("BREAKFAST") && parseInt(timeList[0])>=8 && parseInt(timeList[0])<10) {
                MedicationOuterClass.Take take = MedicationOuterClass.Take.newBuilder().setMsg("Medicament "+nume+" luat de pacientul " + pacient + " cu intake-ul: " + intake + " la ora: "+ time.getText()).build();
                ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost",9090).usePlaintext().build();
                medicamentationGrpc.medicamentationBlockingStub medicamentationStub = medicamentationGrpc.newBlockingStub(channel);
                MedicationOuterClass.Take response = medicamentationStub.taken(take);
                System.out.println(response);
                l.setText("medicament luat");
            } else if(intake.equals("LUNCH") && parseInt(timeList[0])>=11 && parseInt(timeList[0])<14) {
                MedicationOuterClass.Take take = MedicationOuterClass.Take.newBuilder().setMsg("Medicament "+nume+" luat de pacientul " + pacient + " cu intake-ul: " + intake + " la ora: "+ time.getText()).build();
                ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost",9090).usePlaintext().build();
                medicamentationGrpc.medicamentationBlockingStub medicamentationStub = medicamentationGrpc.newBlockingStub(channel);
                MedicationOuterClass.Take response = medicamentationStub.taken(take);
                System.out.println(response);
                l.setText("medicament luat");
            } else if(intake.equals("DINNER") && parseInt(timeList[0])>=16 && parseInt(timeList[0])<18) {
                MedicationOuterClass.Take take = MedicationOuterClass.Take.newBuilder().setMsg("Medicament "+nume+" luat de pacientul " + pacient + " cu intake-ul: " + intake + " la ora: "+ time.getText()).build();
                ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost",9090).usePlaintext().build();
                medicamentationGrpc.medicamentationBlockingStub medicamentationStub = medicamentationGrpc.newBlockingStub(channel);
                MedicationOuterClass.Take response = medicamentationStub.taken(take);
                System.out.println(response);
                l.setText("medicament luat");
            } else if(intake.equals("BEDTIME") && parseInt(timeList[0])>=20 && parseInt(timeList[0])<22) {
                MedicationOuterClass.Take take = MedicationOuterClass.Take.newBuilder().setMsg("Medicament "+nume+" luat de pacientul " + pacient + " cu intake-ul: " + intake + " la ora: "+ time.getText()).build();
                ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost",9090).usePlaintext().build();
                medicamentationGrpc.medicamentationBlockingStub medicamentationStub = medicamentationGrpc.newBlockingStub(channel);
                MedicationOuterClass.Take response = medicamentationStub.taken(take);
                System.out.println(response);
                l.setText("medicament luat");
            }
        }
    }

}
