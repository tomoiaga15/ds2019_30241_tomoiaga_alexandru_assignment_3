import SD.grpc.Buton;
import SD.grpc.MedicationOuterClass;
import SD.grpc.medicamentationGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import static java.lang.Integer.parseInt;

public class GrpcClient {


    public static class Table {
        JFrame f;
        JPanel p;
        JLabel clock;
        String time;
        private MedicationOuterClass.PillResponse pillResponse = null;

        public Table() {


            this.time = "0:0";
            f = new JFrame();
            p = new JPanel();
            clock = new JLabel(time);
            p.add(clock);

            p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));

            ActionListener updateClockAction = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    String[] timeList = time.split(":");
                    timeList[0] = "" + ((parseInt(timeList[0]) + (parseInt(timeList[1]) + 1) / 60) % 24);
                    timeList[1] = "" + ((parseInt(timeList[1]) + 1) % 60);
                    time = "" + timeList[0] + ":" + timeList[1];
                    clock.setText(time);

                    if(time.equals("0:0")){
                        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 9090).usePlaintext().build();
                        medicamentationGrpc.medicamentationBlockingStub medicamentationStub = medicamentationGrpc.newBlockingStub(channel);

                        ArrayList<String> msg = new ArrayList<String>();

                        for(Component c:p.getComponents()){
                            if(c instanceof JPanel) {
                                if(((JLabel)((JPanel) c).getComponent(2)).getText().equals("")){
                                    msg.add("Nu s-a luat la timp " + ((JLabel)((JPanel) c).getComponent(0)).getText());
                                }
                            }
                        }

                        MedicationOuterClass.NotTaken notTaken = MedicationOuterClass.NotTaken.newBuilder().addAllMsg(msg).build();
                        medicamentationStub.notTaken(notTaken);
                    }

                    if (time.equals("0:1")) {
                        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 9090).usePlaintext().build();

                        medicamentationGrpc.medicamentationBlockingStub medicamentationStub = medicamentationGrpc.newBlockingStub(channel);

                        MedicationOuterClass.Date date = MedicationOuterClass.Date.newBuilder().setDate("2019-12-12").build();

                        pillResponse = medicamentationStub.sendDate(date);

                        System.out.println(pillResponse.toString());

                        p.removeAll();
                        p.add(clock);

                        for (MedicationOuterClass.Plan plan : pillResponse.getPlanList()) {
                            for (MedicationOuterClass.Medication medication : plan.getMedicationList()) {
                                JPanel panel = new JPanel();
                                JLabel emp = new JLabel("");
                                JLabel l = new JLabel("Medicamentul " + medication.getName() + " pentru pacientul: " + plan.getPatient() + " cu intake-ul: " + medication.getIntake());
                                JButton b = new JButton("Ia medicament");
                                b.setSize(25, 8);
                                b.addActionListener(new Buton(medication.getIntake(), clock, emp, medication.getName(), plan.getPatient()));
                                panel.add(l);
                                panel.add(b);
                                panel.add(emp);
                                p.add(panel);
                            }
                        }
                    }

                    SwingUtilities.updateComponentTreeUI(f);
                }

            };

            Timer t = new Timer(1000, updateClockAction);
            t.start();

            f.setTitle("Pill dispenser");
            JScrollPane sp = new JScrollPane(p);
            f.add(sp);
            // Frame Size

            f.setSize(720, 480);
            // Frame Visible = true
            f.setVisible(true);

        }

    }

    public static void main(String[] args) {
        Table t = new Table();
    }
}
