package SD.dto;

import java.util.Objects;

public class UserViewDTO {

    private Integer id;
    private String name;
    private String userName;

    public UserViewDTO(Integer id, String name, String userName) {
        this.id = id;
        this.name = name;
        this.userName = userName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserViewDTO that = (UserViewDTO) o;
        return id.equals(that.id) &&
                name.equals(that.name) &&
                userName.equals(that.userName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, userName);
    }
}
