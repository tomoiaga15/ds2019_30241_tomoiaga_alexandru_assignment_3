package SD.dto;

import SD.entities.Role;
import SD.entities.enums.Gender;

import java.util.Date;
import java.util.Objects;

public class UserDTO {

    private Integer id;
    private String name;
    private Date birthdate;
    private Gender gender;
    private String address;
    private String userName;
    private String password;
    private Role role;

    public UserDTO(Integer id, String name, Date birthdate, Gender gender, String address, String userName, String password, Role role) {
        this.id = id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.userName = userName;
        this.password = password;
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTO userDTO = (UserDTO) o;
        return id.equals(userDTO.id) &&
                name.equals(userDTO.name) &&
                Objects.equals(birthdate, userDTO.birthdate) &&
                gender == userDTO.gender &&
                Objects.equals(address, userDTO.address) &&
                userName.equals(userDTO.userName) &&
                password.equals(userDTO.password) &&
                Objects.equals(role, userDTO.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, birthdate, gender, address, userName, password, role);
    }
}
