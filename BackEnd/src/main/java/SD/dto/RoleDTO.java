package SD.dto;

import SD.entities.User;

import java.util.List;
import java.util.Objects;

public class RoleDTO {

    private Integer id;
    private String type;

    public RoleDTO(Integer id, String type) {
        this.id = id;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleDTO roleDTO = (RoleDTO) o;
        return Objects.equals(id, roleDTO.id) &&
                Objects.equals(type, roleDTO.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type);
    }
}
