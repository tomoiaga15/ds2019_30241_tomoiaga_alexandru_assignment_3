package SD.dto.builders;

import SD.dto.ActivityDTO;
import SD.entities.Activity;

public class ActivityBuilder {

    public static ActivityDTO generateDTOFromEntity(Activity activity){
        return new ActivityDTO(
                activity.getId(),
                activity.getPatient(),
                activity.getStartTime(),
                activity.getEndTime(),
                activity.getActivityName()
        );
    }

    public static Activity generateEntityFromDTO(ActivityDTO activityDTO){
        return new Activity(
                activityDTO.getId(),
                activityDTO.getPatient(),
                activityDTO.getStartTime(),
                activityDTO.getEndTime(),
                activityDTO.getActivityName()
        );
    }
}
