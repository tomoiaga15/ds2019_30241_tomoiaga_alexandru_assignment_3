package SD.dto.builders;

import SD.dto.UserViewDTO;
import SD.entities.User;

public class UserViewBuilder {

    public UserViewBuilder() {
    }

    public static UserViewDTO generateDTOFromEntity(User user){
        return new UserViewDTO(
                user.getId(),
                user.getName(),
                user.getUserName());
    }

    public static User generateEntityFromDTO(UserViewDTO userViewDTO){
        return new User(
                userViewDTO.getId(),
                userViewDTO.getName(),
                userViewDTO.getUserName());
    }
}
