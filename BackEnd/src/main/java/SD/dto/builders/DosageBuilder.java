package SD.dto.builders;

import SD.dto.DosageDTO;
import SD.entities.Dosage;

public class DosageBuilder {

    public DosageBuilder() {
    }

    public static DosageDTO generateDTOFromEntity(Dosage dosage){
        return new DosageDTO(
                dosage.getId(),
                dosage.getIntake(),
                dosage.getMedicationPerPlan(),
                dosage.getUnit()
        );
    }

    public static Dosage generateEntityFromDTO(DosageDTO dosageDTO){
        return new Dosage(
                dosageDTO.getId(),
                dosageDTO.getIntake(),
                dosageDTO.getMedicationPerPlan(),
                dosageDTO.getUnit()
        );
    }
}
