package SD.dto.builders;

import SD.dto.UserDTO;
import SD.entities.User;

public class UserBuilder {

    public UserBuilder() {
    }

    public static UserDTO generateDTOFromEntity(User user){
        return new UserDTO(
                user.getId(),
                user.getName(),
                user.getBirthdate(),
                user.getGender(),
                user.getAddress(),
                user.getUserName(),
                user.getPassword(),
                user.getRole());
    }

    public static User generateEntityFromDTO(UserDTO userDTO){
        return new User(
                userDTO.getId(),
                userDTO.getName(),
                userDTO.getBirthdate(),
                userDTO.getGender(),
                userDTO.getAddress(),
                userDTO.getUserName(),
                userDTO.getPassword(),
                userDTO.getRole());
    }
}
