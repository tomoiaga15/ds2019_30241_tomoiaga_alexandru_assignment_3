package SD.dto.builders;

import SD.dto.MedicationPerPlanDTO;
import SD.entities.MedicationPerPlan;

public class MedicationPerPlanBuilder {

    public MedicationPerPlanBuilder() {
    }

    public static MedicationPerPlanDTO generateDTOFromEntity(MedicationPerPlan medicationPerPlan){
        return new MedicationPerPlanDTO(
                medicationPerPlan.getId(),
                medicationPerPlan.getMedicationPlan(),
                medicationPerPlan.getMedication()
        );
    }

    public static MedicationPerPlan generateEntityFromDTO(MedicationPerPlanDTO medicationPerPlanDTO){
        return new MedicationPerPlan(
                medicationPerPlanDTO.getId(),
                medicationPerPlanDTO.getMedicationPlan(),
                medicationPerPlanDTO.getMedication()
        );
    }
}
