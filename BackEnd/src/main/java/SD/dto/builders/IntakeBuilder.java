package SD.dto.builders;

import SD.dto.IntakeDTO;
import SD.entities.Intake;

public class IntakeBuilder {

    public IntakeBuilder() {
    }

    public static IntakeDTO generateDTOFromEntity(Intake intake){
        return new IntakeDTO(
          intake.getId(),
          intake.getIntakeMoment()
        );
    }

    public static Intake generateEntityFromDTO(IntakeDTO intakeDTO){
        return new Intake(
          intakeDTO.getId(),
          intakeDTO.getIntakeMoment()
        );
    }
}
