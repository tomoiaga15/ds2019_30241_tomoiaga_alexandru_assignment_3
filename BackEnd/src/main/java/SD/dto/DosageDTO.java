package SD.dto;

import SD.entities.Intake;
import SD.entities.MedicationPerPlan;

import java.util.Objects;

public class DosageDTO {

    private Integer id;
    private Intake intake;
    private MedicationPerPlan medicationPerPlan;
    private Integer unit;

    public DosageDTO() {
    }

    public DosageDTO(Integer id, Intake intake, MedicationPerPlan medicationPerPlan, Integer unit) {
        this.id = id;
        this.intake = intake;
        this.medicationPerPlan = medicationPerPlan;
        this.unit = unit;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Intake getIntake() {
        return intake;
    }

    public void setIntake(Intake intake) {
        this.intake = intake;
    }

    public MedicationPerPlan getMedicationPerPlan() {
        return medicationPerPlan;
    }

    public void setMedicationPerPlan(MedicationPerPlan medicationPerPlan) {
        this.medicationPerPlan = medicationPerPlan;
    }

    public Integer getUnit() {
        return unit;
    }

    public void setUnit(Integer unit) {
        this.unit = unit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DosageDTO dosageDTO = (DosageDTO) o;
        return Objects.equals(id, dosageDTO.id) &&
                Objects.equals(intake, dosageDTO.intake) &&
                Objects.equals(medicationPerPlan, dosageDTO.medicationPerPlan) &&
                Objects.equals(unit, dosageDTO.unit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, intake, medicationPerPlan, unit);
    }
}
