package SD.dto;

import SD.entities.enums.IntakeMoment;

import java.util.Objects;

public class IntakeDTO {

    private Integer id;

    private IntakeMoment intakeMoment;

    public IntakeDTO(Integer id, IntakeMoment intakeMoment) {
        this.id = id;
        this.intakeMoment = intakeMoment;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public IntakeMoment getIntakeMoment() {
        return intakeMoment;
    }

    public void setIntakeMoment(IntakeMoment intakeMoment) {
        this.intakeMoment = intakeMoment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntakeDTO intakeDTO = (IntakeDTO) o;
        return id.equals(intakeDTO.id) &&
                intakeMoment == intakeDTO.intakeMoment;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, intakeMoment);
    }
}
