package SD.dto;

import SD.entities.User;

import java.util.Objects;

public class PatientDTO {

    private Integer id;
    private User user;
    private User doctor;
    private User caregiver;
    private String medicalRecord;

    public PatientDTO(Integer id, User user, User doctor, User caregiver, String medicalRecord) {
        this.id = id;
        this.user = user;
        this.doctor = doctor;
        this.caregiver = caregiver;
        this.medicalRecord = medicalRecord;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getDoctor() {
        return doctor;
    }

    public void setDoctor(User doctor) {
        this.doctor = doctor;
    }

    public User getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(User caregiver) {
        this.caregiver = caregiver;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientDTO that = (PatientDTO) o;
        return id.equals(that.id) &&
                user.equals(that.user) &&
                doctor.equals(that.doctor) &&
                caregiver.equals(that.caregiver) &&
                Objects.equals(medicalRecord, that.medicalRecord);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, doctor, caregiver, medicalRecord);
    }
}


