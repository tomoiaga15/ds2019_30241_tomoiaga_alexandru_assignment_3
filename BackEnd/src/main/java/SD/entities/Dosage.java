package SD.entities;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class Dosage {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "intake_id")
    private Intake intake;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "medicationPerPlan_id")
    private MedicationPerPlan medicationPerPlan;

    @Column(name = "unit", nullable = false, length = 100)
    private Integer unit;

    public Dosage() {
    }

    public Dosage(Integer id, Intake intake, MedicationPerPlan medicationPerPlan, Integer unit) {
        this.id = id;
        this.intake = intake;
        this.medicationPerPlan = medicationPerPlan;
        this.unit = unit;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Intake getIntake() {
        return intake;
    }

    public void setIntake(Intake intake) {
        this.intake = intake;
    }

    public MedicationPerPlan getMedicationPerPlan() {
        return medicationPerPlan;
    }

    public void setMedicationPerPlan(MedicationPerPlan medicationPerPlan) {
        this.medicationPerPlan = medicationPerPlan;
    }

    public Integer getUnit() {
        return unit;
    }

    public void setUnit(Integer unit) {
        this.unit = unit;
    }
}
