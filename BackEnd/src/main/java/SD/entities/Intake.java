package SD.entities;

import SD.entities.enums.IntakeMoment;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class Intake {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "moment", nullable = false)
    private IntakeMoment intakeMoment;


    public Intake() {
    }

    public Intake(Integer id, IntakeMoment intakeMoment) {
        this.id = id;
        this.intakeMoment = intakeMoment;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public IntakeMoment getIntakeMoment() {
        return intakeMoment;
    }

    public void setIntakeMoment(IntakeMoment intakeMoment) {
        this.intakeMoment = intakeMoment;
    }

}
