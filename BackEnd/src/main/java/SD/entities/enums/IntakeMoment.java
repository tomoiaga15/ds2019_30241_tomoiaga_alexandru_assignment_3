package SD.entities.enums;

public enum IntakeMoment {
    BREAKFAST,
    LUNCH,
    DINNER,
    BEDTIME
}
