package SD.services;

import SD.dto.ActivityDTO;
import SD.dto.builders.ActivityBuilder;
import SD.repositories.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityService {

    @Autowired
    private final ActivityRepository activityRepository;

    public ActivityService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public Integer insert(ActivityDTO activityDTO) {
        return activityRepository.save(ActivityBuilder.generateEntityFromDTO(activityDTO)).getId();
    }
}
