package SD.services;

import SD.dto.DosageDTO;
import SD.dto.builders.DosageBuilder;
import SD.entities.Dosage;
import SD.repositories.DosageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DosageService {

    @Autowired
    private final DosageRepository dosageRepository;

    public DosageService(DosageRepository dosageRepository) {
        this.dosageRepository = dosageRepository;
    }

    public DosageDTO findById(Integer id){
        Optional<Dosage> dosage = dosageRepository.findById(id);

        if(!dosage.isPresent()){
            throw new ResourceNotFoundException();
        }
        return DosageBuilder.generateDTOFromEntity(dosage.get());
    }

    public List<DosageDTO> getDosageForMPId(Integer id) {
        List<Dosage> medications = dosageRepository.getDosageForMPId(id);
        return medications.stream()
                .map(DosageBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public List<DosageDTO> findAll(){
        List<Dosage> medications = dosageRepository.findAll();
        return medications.stream()
                .map(DosageBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(DosageDTO dosageDTO) {
        return dosageRepository.save(DosageBuilder.generateEntityFromDTO(dosageDTO)).getId();
    }

    public Integer update(DosageDTO dosageDTO){
        Optional<Dosage> dosage = dosageRepository.findById(dosageDTO.getId());
        if(!dosage.isPresent()){
            throw new ResourceNotFoundException();
        }
        return dosageRepository.save(DosageBuilder.generateEntityFromDTO(dosageDTO)).getId();
    }

    public void delete(Integer id){
        this.dosageRepository.deleteById(id);
    }
}
