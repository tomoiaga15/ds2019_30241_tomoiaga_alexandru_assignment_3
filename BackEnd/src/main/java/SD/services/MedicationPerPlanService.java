package SD.services;

import SD.dto.MedicationPerPlanDTO;
import SD.dto.builders.MedicationPerPlanBuilder;
import SD.entities.MedicationPerPlan;
import SD.repositories.MedicationPerPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationPerPlanService {

    @Autowired
    private final MedicationPerPlanRepository medicationPerPlanRepository;

    public MedicationPerPlanService(MedicationPerPlanRepository medicationPerPlanRepository) {
        this.medicationPerPlanRepository = medicationPerPlanRepository;
    }

    public MedicationPerPlanDTO findById(Integer id){
        Optional<MedicationPerPlan> medicationPerPlan = medicationPerPlanRepository.findById(id);

        if(!medicationPerPlan.isPresent()){
            throw new ResourceNotFoundException();
        }
        return MedicationPerPlanBuilder.generateDTOFromEntity(medicationPerPlan.get());
    }

    public List<MedicationPerPlanDTO> findByMedicationPlanId(Integer id) {
        List<MedicationPerPlan> medications = medicationPerPlanRepository.findAllMPPForMP(id);
        return medications.stream()
                .map(MedicationPerPlanBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public List<MedicationPerPlanDTO> findAll(){
        List<MedicationPerPlan> medications = medicationPerPlanRepository.findAll();
        return medications.stream()
                .map(MedicationPerPlanBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(MedicationPerPlanDTO medicationPerPlanDTO) {
        return medicationPerPlanRepository.save(MedicationPerPlanBuilder.generateEntityFromDTO(medicationPerPlanDTO)).getId();
    }

    public Integer update(MedicationPerPlanDTO medicationPerPlanDTO){
        Optional<MedicationPerPlan> medicationPerPlan = medicationPerPlanRepository.findById(medicationPerPlanDTO.getId());
        if(!medicationPerPlan.isPresent()){
            throw new ResourceNotFoundException();
        }
        return medicationPerPlanRepository.save(MedicationPerPlanBuilder.generateEntityFromDTO(medicationPerPlanDTO)).getId();
    }

    public void delete(Integer id){
        this.medicationPerPlanRepository.deleteById(id);
    }
}
