package SD.repositories;

import SD.entities.MedicationPerPlan;
import SD.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface MedicationPerPlanRepository extends JpaRepository<MedicationPerPlan, Integer> {

    @Query(value = "SELECT m " +
            "FROM MedicationPerPlan m " +
            "WHERE m.medicationPlan.id = ?1")
    List<MedicationPerPlan> findAllMPPForMP(Integer medicationPlanId);
}
