package SD.repositories;

import SD.entities.Intake;
import SD.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface IntakeRepository extends JpaRepository<Intake, Integer> {

}
