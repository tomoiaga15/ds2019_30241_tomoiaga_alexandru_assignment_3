package SD.repositories;

import SD.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface PatientRepository extends JpaRepository<Patient, Integer> {

    Patient findByUserId(Integer userId);

    @Query(value = "SELECT p " +
            "FROM Patient p " +
            "WHERE p.caregiver.id = ?1")
    List<Patient> findAllPatietsForCaregiver(Integer caregiverId);

    @Query(value = "SELECT p " +
            "FROM Patient p " +
            "WHERE p.doctor.id = ?1")
    List<Patient> findAllPatietsForDoctor(Integer doctorId);
}
