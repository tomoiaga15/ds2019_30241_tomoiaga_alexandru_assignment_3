package SD.repositories;

import SD.entities.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Integer> {

    MedicationPlan findByPatientId(Integer patientId);
}
