package SD.controller;

import SD.dto.PatientDTO;
import SD.services.PatientService;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping()
    public List<PatientDTO> findAll(){
        return patientService.findAll();
    }

    @GetMapping(value="/doctor/{id}")
    public List<PatientDTO> findAllforDoctor(@PathVariable("id") Integer id){
        return patientService.findAllPatietsForDoctor(id);
    }

    @GetMapping(value="/caregiver/{id}")
    public List<PatientDTO> findAllforCaregiver(@PathVariable("id") Integer id){
        return patientService.findAllPatietsForCaregiver(id);
    }

    @GetMapping(value="/user/{id}")
    public PatientDTO findByUserId(@PathVariable("id") Integer id){
        return patientService.findByUserId(id);
    }

    @GetMapping(value = "/{id}")
    public PatientDTO findById(@PathVariable("id") Integer id){
        return patientService.findById(id);
    }

    @PostMapping()
    public Integer insert(@Valid @RequestBody PatientDTO patientDTO) throws DuplicateName {
        return patientService.insert(patientDTO);
    }

    @PutMapping()
    public Integer update(@Valid @RequestBody PatientDTO patientDTO){
        return patientService.update(patientDTO);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Integer id){
        patientService.delete(id);
    }
}
