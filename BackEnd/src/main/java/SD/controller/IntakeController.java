package SD.controller;

import SD.dto.IntakeDTO;
import SD.services.IntakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/intake")
public class IntakeController {

    private final IntakeService intakeService;

    @Autowired
    public IntakeController(IntakeService intakeService) {
        this.intakeService = intakeService;
    }

    @GetMapping(value = "/{id}")
    public IntakeDTO findById(@PathVariable("id") Integer id){
        return intakeService.findById(id);
    }

}
