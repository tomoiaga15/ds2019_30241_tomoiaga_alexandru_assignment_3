package SD.controller;

import SD.dto.MedicationPlanDTO;
import SD.services.MedicationPlanService;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicationplan")
public class MedicationPlanController {

    private final MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService) {
        this.medicationPlanService = medicationPlanService;
    }

    @GetMapping()
    public List<MedicationPlanDTO> findAll(){
        return medicationPlanService.findAll();
    }

    @GetMapping(value = "/{id}")
    public MedicationPlanDTO findById(@PathVariable("id") Integer id){
        return medicationPlanService.findById(id);
    }

    @GetMapping(value = "/patient/{id}")
    public MedicationPlanDTO findByPatientId(@PathVariable("id") Integer id){
        return medicationPlanService.findByPatientId(id);
    }

    @PostMapping()
    public Integer insert(@Valid @RequestBody MedicationPlanDTO medicationPlanDTO) throws DuplicateName {
        return medicationPlanService.insert(medicationPlanDTO);
    }

    @PutMapping()
    public Integer update(@Valid @RequestBody MedicationPlanDTO medicationPlanDTO){
        return medicationPlanService.update(medicationPlanDTO);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Integer id){
        medicationPlanService.delete(id);
    }
}
